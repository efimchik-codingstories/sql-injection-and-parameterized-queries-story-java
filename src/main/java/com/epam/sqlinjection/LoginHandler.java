package com.epam.sqlinjection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.OptionalLong;

public class LoginHandler {

    public OptionalLong login(Connection sqlConnection, String username, String password) throws SQLException {
        final PreparedStatement statement = sqlConnection.prepareStatement(
                "select user_id from user " +
                        "where username = ? and userpass = ?"
        );
        statement.setString(1, username);
        statement.setString(2, password);
        final ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            return OptionalLong.of(resultSet.getLong("user_id"));
        }
        return OptionalLong.empty();
    }
}
